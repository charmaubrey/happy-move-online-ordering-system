import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { Page404Component } from './page404/page404.component';


const routes: Routes = [
  { 
  	path: 'login',
  	component: LoginComponent,
  	data: { title: 'Login' } 
  },
  { 
  	path: 'register',      
  	component: RegisterComponent,
  	data: { title: 'Register' }
  },
  { 
    path: 'forgot-password',      
    component: ForgotPasswordComponent,
    data: { title: 'Forgot Password' }
  },
  { path: 'main', loadChildren: () => import('./main/main.module').then(m => m.MainModule) }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }