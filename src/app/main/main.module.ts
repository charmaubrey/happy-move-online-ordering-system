import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolsModule } from '../../tools/tools.module';

import { MainRoutingModule } from './main-routing.module';

import { MainComponent } from './main.component';
import { BookingListComponent } from './booking-list/booking-list.component';
import { WalletLedgerComponent } from './wallet-ledger/wallet-ledger.component';
import { ChargeLedgerComponent } from './charge-ledger/charge-ledger.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { Page404mainComponent } from './page404main/page404main.component';

@NgModule({
  declarations: [
  	MainComponent,
  	BookingListComponent,
  	WalletLedgerComponent,
    ChargeLedgerComponent,
    UpdateProfileComponent,
    ChangePasswordComponent,
    Page404mainComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    ToolsModule
  ],
  bootstrap: [MainComponent]
})
export class MainModule { }
