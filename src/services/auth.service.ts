import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';

declare var require: any;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public currentUser: any;

  constructor(private _http: HttpClient) {
  }

  currentUserValue() {
    this.currentUser = this.decryptcustomerKey();
    return this.currentUser;
  }

  login(email: string, password: string) {

      let data = {
        "username": email,
        "password": password
      };

      var headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json; charset=utf-8' });

      return this._http.get(environment.backendserver.login + '?username=' + data.username + '&password=' + data.password, { headers: headers })
        .pipe(map(user => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes

          let headers1 = new HttpHeaders();
          headers1.append('Content-Type', 'application/json; charset=utf-8');
          headers1.append('Access-Control-Allow-Origin', '*');
          headers1.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
          let data1 = { id: user['customerKey'], username: user['email'], password: data.password };

          this._http.post('http://52.221.196.138:8787/api/Users/AuthenticateCustomer', data1, { headers: headers1 })
            .subscribe(result => {

              var CryptoJS = require("crypto-js");
              var storedData = [{ token: result['token'] }, { customerKey: user['customerKey'] }, { walletType: user['walletType'] }, { merchantKey: user['merchantId'] }]

              var encryptData = CryptoJS.AES.encrypt(JSON.stringify(storedData), environment.backendserver.secretKey);
              localStorage.setItem('lg_id', encryptData);

              return user;
            })

        }));
    // this._http.get(environment.backendserver.login + '?username=' + data.username + '&password=' + data.password, { headers: headers })
    //   .subscribe(res => {
    //     localStorage.setItem('token',res['token']);
    //     return res['token'];
    //   })
  }

  decryptmerchantId() {
    var CryptoJS = require("crypto-js");
    var storedData = localStorage.getItem('lg_id');

    var bytes = CryptoJS.AES.decrypt(storedData.toString(), environment.backendserver.secretKey);
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    return decryptedData[3].merchantKey;
  }

  decryptwalletType() {
    var CryptoJS = require("crypto-js");
    var storedData = localStorage.getItem('lg_id');

    var bytes = CryptoJS.AES.decrypt(storedData.toString(), environment.backendserver.secretKey);
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    return decryptedData[2].walletType;
  }

  decryptcustomerKey() {
    var CryptoJS = require("crypto-js");
    var storedData = localStorage.getItem('lg_id');

    if (storedData != null) {
      var bytes = CryptoJS.AES.decrypt(storedData.toString(), environment.backendserver.secretKey);
      var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

      return decryptedData[1].customerKey;
    }
    else {
      return null;
    }
  }

  decryptToken() {
    var CryptoJS = require("crypto-js");
    var storedData = localStorage.getItem('lg_id');

    var bytes = CryptoJS.AES.decrypt(storedData.toString(), environment.backendserver.secretKey);
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    return decryptedData[0].token;
  }

  logout() {
    localStorage.clear();
  }
}
