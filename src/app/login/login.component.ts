import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/services/auth.service';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  register: any;
  token: any;
  customer: any;
  walletType: any;
  merchantKey: any;

  color = 'primary';
  mode = 'indeterminate';
  value = 30;

  message: string = 'Invalid Credentials.';
  actionButtonLabel: string = '';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 2000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  addExtraClass: boolean = true;

  constructor(
    private _router: Router,
    private _http: HttpClient,
    private _authService: AuthService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  signup() {
    this._router.navigate(['register']);
  }

  forgot(){
    this._router.navigate(['forgot-password']);
  }

  handleError(err: HttpErrorResponse) {
    if (err.status == 500) {

      console.log('err');

    } else if (err.status == 400) {

      return Observable.throw(new Error('AD'));
    }

    // you can also catch all errors of a group like this
    else if (err.status < 500 && err.status >= 400) {

      return Observable.throw(new Error('ASDASd'));
    }
  }


  login() {
    if (this.email === undefined || this.password === undefined || this.email === '' || this.password === '') {
      let config = new MatSnackBarConfig();
      config.duration = 1000;
      config.verticalPosition = this.verticalPosition;
      config.horizontalPosition = this.horizontalPosition;
      config.panelClass = ['error']
      this._snackBar.open("Please enter email and password", "", config);
      console.clear();

    }
    else {
      try {

      
      this._authService.login(this.email, this.password)
        .subscribe(data => {
          this._router.navigate(['main/map/booking']);
        }, (err) => {
          
          let config = new MatSnackBarConfig();
          config.verticalPosition = this.verticalPosition;
          config.horizontalPosition = this.horizontalPosition;
          config.duration = this.setAutoHide ? this.autoHide : 0;
          config.panelClass = this.addExtraClass ? ['error'] : undefined;
          this._snackBar.open(this.message, this.action ? this.actionButtonLabel : undefined, config);
          console.clear();

        });
      } 
      catch {
        console.clear();
      }
    }
    // 	console.log(this.email);
    // 	console.log(this.password);

    //  let data = {
    //   "username": this.email,
    //   "password": this.password
    // };

    // var headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json; charset=utf-8' });

    // this._http.get(environment.backendserver.login + '?username=' + data.username + '&password=' + data.password, { headers: headers })
    //   .subscribe(res => {


    //     let headers1 = new HttpHeaders();
    //     headers1.append('Content-Type', 'application/json; charset=utf-8');
    //     headers1.append('Access-Control-Allow-Origin', '*');
    //     headers1.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    //     let data1 = { id: res['customerKey'], username: res['email'], password: data.password };

    //     this._http.post('http://52.221.196.138:8787/api/Users/AuthenticateCustomer', data1, { headers: headers1 })
    //       .subscribe(result => {

    //         this.customer = res['customerKey'];
    //         this.walletType = res['walletType'];
    //         this.merchantKey = res['merchantId'];

    //         localStorage.setItem('token',result['token']);
    //         localStorage.setItem('customerKey', this.customer);
    //         localStorage.setItem('walletType', this.walletType);
    //         localStorage.setItem('merchantKey', this.merchantKey);

    //         this._router.navigate(['main/map/booking']);

    //        },(err)=> {
    //          console.log(err);
    //        })


    //   }, (err) => {
    //     alert("Invalid Credentials");
    //   })
  }

}
