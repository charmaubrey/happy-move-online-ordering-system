import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './map.component';
import { AgmCoreModule } from '@agm/core';
import { MapRoutingModule } from './map-routing.module';
import { BookingComponent } from './booking/booking.component';
import { ToolsModule } from 'src/tools/tools.module';
import { BookingDetailsComponent } from './booking-details/booking-details.component';


@NgModule({
  declarations: [
  	MapComponent,
  	BookingComponent,
  	BookingDetailsComponent
  ],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyDFTKbcSXEN22pUx3zfaabEOGyy7oOZtmI',
      libraries: ['places']
    }),
    MapRoutingModule,
    ToolsModule
  ],
  exports: [ToolsModule],
  bootstrap: [MapComponent]
})
export class MapModule { }
