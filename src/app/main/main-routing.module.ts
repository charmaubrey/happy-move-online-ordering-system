import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main.component';
import { BookingListComponent } from './booking-list/booking-list.component';
import { WalletLedgerComponent } from './wallet-ledger/wallet-ledger.component';
import { ChargeLedgerComponent } from './charge-ledger/charge-ledger.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { Page404mainComponent } from './page404main/page404main.component';
import { AuthguardGuard } from 'src/guards/authguard.guard';



const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      { path: 'booking-list', component: BookingListComponent, canActivate: [AuthguardGuard]},
    	{ path: 'wallet-ledger', component: WalletLedgerComponent },
    	{ path: 'charge-ledger', component: ChargeLedgerComponent },
    	{ path: 'change-password', component: ChangePasswordComponent },
      { path: 'update-profile', component: UpdateProfileComponent },
      { path: 'page404main', component: Page404mainComponent },
      { path: 'map', loadChildren: () => import('./map/map.module').then(m => m.MapModule) },
      { path: '**', redirectTo:'page404main'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
