import { Component, OnInit, ViewChild, TemplateRef, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/services/auth.service';

declare var google: any;

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.scss']
})
export class BookingDetailsComponent implements OnInit {
  @Output() output = new EventEmitter<string>();
  @Output() outputDriver = new EventEmitter<any>();
  @Input() input: any;
  @Input() driverLoc: any;

  state: any;
  items: any;
  token: any;

  panelOpenState = false;
  color = 'primary';
  mode = 'indeterminate';
  value = 50;

  status: any;
  driverPicture: any;
  deliveryTime: any;
  totalGross: any;
  deliveryStops: any;
  deliveryFireBaseID: any;
  myReason: string;
  reasons: string[] = ['Duplicate Order', 'Found Alternative', 'Waited Too Long', 'Wrong Booking Information'];
  myFee: string;
  fees: string[] = ['20', '50', '100', '200'];
  other: any;
  paymentType: any;
  walletType: any;
  merchantKey: any;
  customersKey: any;
  currentDate: any;
  promoCode: any;
  driverName: any;
  driverMobile: any;
  driverVehicle: any;
  driverRating: any;
  vehicleTypeID: any;

  /* Price Breakdown Variables */
  standardRate: any;
  additionalStopsCharge: any;
  cashRemittance: any;
  cashHandling: any;
  discountValue: any;
  driverCarries: any;
  holiday: any;
  insulatedBox: any;
  priorityFee: any;
  purchaseService: any;
  queuingService: any;
  totalDeliveryFee: any;
  itemPrice: any;
  totalETA: any;
  interval: any;
  interval2: any;
  afterHours: any;
  mvxs: any;
  additionalAssistant: any;
  additionalAssistant2: any;
  driverService: any;


  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  iscancelled: any;
  iscancelled2: any;
  thisTimers: any;
  min: any;
  eta_mins: any;

  driverLat: any;
  driverLng: any;
  pickupLat: any;
  pickupLng: any;
  dropoffLat: any;
  dropoffLng: any;

  constructor(
    private _db: AngularFireDatabase,
    private _http: HttpClient,
    private dialog: MatDialog,
    private _router: Router,
    private _authService: AuthService
  ) {
    clearInterval(this.interval);
    this.iscancelled = true;
    this.iscancelled2 = true;
  }

  ngOnInit() {
    this.state = window.history.state.deliveryFireBaseID;
    this.loadDeliveryDetails(this.state);
  }

  loadDeliveryDetails(deliveryFireBaseID) {
    var customersKey = this._authService.decryptcustomerKey();
    this.customersKey = customersKey;


    this._db.object('deliveries/' + customersKey + '/' + deliveryFireBaseID).valueChanges()
      .subscribe(res => {
        this.input = res['deliveryStops'];

        if (res['driverLat'] === undefined && res['driverLng'] === undefined) {
          this.driverLoc = JSON.parse("[0,0]");
        }
        else {
          this.driverLoc = JSON.parse("[" + res['driverLat'] + ',' + res['driverLng'] + ',' + res['vehicleTypeID'] + "]");
        }

        this.status = res['status'];
        this.driverPicture = res['driverPicture'];
        this.driverName = res['driverName'];
        this.deliveryTime = res['deliveryTime'];
        this.totalGross = res['totalGross'];
        this.deliveryStops = res['deliveryStops'];
        this.deliveryFireBaseID = res['deliveryFireBaseID'];
        this.customersKey = res['customersKey'];
        this.paymentType = res['paymentType'];
        this.promoCode = res['promoCode'];
        this.standardRate = res['standardRate'];
        this.additionalStopsCharge = res['additionalStopsCharge'];
        this.cashRemittance = res['cashRemittance'];
        this.cashHandling = res['cashHandling'];
        this.discountValue = res['discountValue'];
        this.driverCarries = res['driverCarries'];
        this.holiday = res['holiday'];
        this.insulatedBox = res['insulatedBox'];
        this.priorityFee = res['priorityFee'];
        this.purchaseService = res['purchaseService'];
        this.queuingService = res['queuingService'];
        this.totalDeliveryFee = res['totalDeliveryFee'];
        this.itemPrice = res['itemPrice'];
        this.driverMobile = res['driverMobile'];
        this.driverVehicle = res['driverVehicle'];
        this.driverRating = res['driverRating'];
        this.vehicleTypeID = res['vehicleTypeID'];
        this.afterHours = res['afterHours'];
        this.mvxs = res['mvxs'];
        this.additionalAssistant = res['additionalAssistant'];
        this.additionalAssistant2 = res['additionalAssistant2'];
        this.itemPrice = res['itemPrice'];

        this.passStopsToMaps();
        this.passDriverLoctoMaps();

        if (this.iscancelled === true && res['driverLat'] != 0 && res['driverLng'] != 0 && res['status'] != '1') {
          this.iscancelled = false;
          this.getETA();
        }

      })

  }

  closeDetails() {
    this.dialog.closeAll();
  }

  getETA() {

    clearInterval(this.interval);
    this._db.object('deliveries/' + this.customersKey + '/' + this.deliveryFireBaseID).valueChanges()
      .subscribe(list => {
        this.driverLat = list['driverLat'];
        this.driverLng = list['driverLng'];
        this.pickupLat = list['deliveryStops'][0].lat;
        this.pickupLng = list['deliveryStops'][0].lng;
        this.dropoffLat = list['deliveryStops'][list['deliveryStops'].length - 1].lat;
        this.dropoffLng = list['deliveryStops'][list['deliveryStops'].length - 1].lng;
      })



    this._db.object('deliveries/' + this.customersKey + '/' + this.deliveryFireBaseID + '/status').valueChanges()
      .subscribe(data => {

        clearInterval(this.interval);
        var status = data;
        let message_pickup = 'ETA to Pickup: ';
        let message_laststop = 'ETA to Last Stop: ';

        if (this.driverLat != 0 && this.driverLng != 0) {
          if (status == 6 || status === '6') {
            this.loadGoogleETA('' + this.driverLat + ',' + this.driverLng + '', '' + this.pickupLat + ',' + this.pickupLng + '', message_pickup);
          } else if (status == 9 || status === '9' || status == 2 || status === '2') {
            clearInterval(this.interval);
            this.loadGoogleETA('' + this.driverLat + ',' + this.driverLng + '', '' + this.dropoffLat + ',' + this.dropoffLng + '', message_laststop);
            // console.log(data['deliveryStops'][data['deliveryStops'].length - 1].stop);
          } else { // if cancelled or completed
            clearInterval(this.interval);
          }
        }
      })
  }

  loadGoogleETA(driverLngLat, locLngLat, message) {

    clearInterval(this.interval);

    var request = {
      origin: driverLngLat,
      destination: locLngLat,
      travelMode: google.maps.DirectionsTravelMode.DRIVING,
      optimizeWaypoints: true,
      provideRouteAlternatives: true,
    };

    this.directionsService.route(request, (response, status) => {
      if (status == google.maps.DirectionsStatus.OK) {
       
        let eta = response.routes[0].legs[0].duration.value;
        this.startTimer(eta, message);

      } else {
        console.log('status not ok');
      }
    });
  }

  startTimer(eta, message) {
    clearInterval(this.interval);
    try {
      this.interval = setInterval(() => {
        eta = eta - 1;
        this.eta_mins = eta / 60;

        if (eta == 120 || eta == 300 || eta == 600) { // if 2 mins/ 5mins/ 10mins
          clearInterval(this.interval);

          this.getETA();
        } else if (eta < 60) {
          clearInterval(this.interval);
          this.thisTimers = message;
          this.min = '1 min';
        } else {

          this.thisTimers = message;
          this.min = Number(parseInt(this.eta_mins)) + ' mins';
        }

      }, 1000);
    } catch (e) {
      this.thisTimers = '...';
      console.log('....');
    }

  }

  ngOnDestroy() {
    clearInterval(this.interval);
    this.iscancelled = false;
    this.iscancelled2 = false;
  }

  passDriverLoctoMaps() {
    this.outputDriver.emit(this.driverLoc);
  }

  passStopsToMaps() {
    this.output.emit(JSON.stringify(this.input));
  }

  priceDetails(ref: TemplateRef<any>) {
    this.dialog.open(ref, { disableClose: false, width: '450px' });
  }

  submitPriorityFee(ref: TemplateRef<any>) {
    if (this.myFee === undefined || this.myFee === '') {
      alert('select priority amount');
    } else {

      this.dialog.closeAll();
      this.dialog.open(ref, { disableClose: true });

      this.token = this._authService.decryptToken();
      this.totalGross = Number(this.totalGross) + Number(this.myFee);

      let data = {
        "deliveryFireBaseID": this.deliveryFireBaseID,
        "priorityFee": this.myFee,
        "totalGross": this.totalGross,
        "paymentType": this.paymentType
      };

      var headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + this.token });

      let json = JSON.stringify(data);
      let url = environment.backendserver.updateDeliveryPriorityFee;

      this._http.post(url, data, { headers: headers })
        .subscribe(res => {

          console.log(res);
          if (res['message'] === 'success') {
            this._db.object('deliveries/' + this.customersKey + '/' + this.deliveryFireBaseID).update({
              priorityFee: this.myFee,
              totalGross: this.totalGross,
              totalDeliveryFee: this.totalDeliveryFee
            })

            this.dialog.closeAll();
          }
        })

    }
  }

  closePriorityFee() {
    this.dialog.closeAll();
  }

  submit(ref: TemplateRef<any>) {
    if (this.myReason === undefined || this.myReason === '') {
      alert('select the ff reason');

    }
    else {

      this.dialog.closeAll();
      this.dialog.open(ref, { disableClose: true });

      this.walletType = this._authService.decryptwalletType();
      this.merchantKey = this._authService.decryptmerchantId();
      this.token = this._authService.decryptToken();

      var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
      var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      var dateday = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
      var d = new Date();
      var dayName = days[d.getDay()];
      var monthNae = months[d.getMonth()];
      var asdx = ('0' + d.getDate()).slice(-2);
      var year = d.getFullYear();
      var mins = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();

      var currendate = (dayName + ' ' + monthNae + ' ' + asdx + ' ' + year + ' ' + d.getHours() + ':' + mins);

      let data = {
        "deliveryFireBaseID": this.deliveryFireBaseID,
        "status": 5,
        "cancellationReason": this.myReason.replace(/['`"‘’“”]/g, ' '),
        "walletType": parseInt(this.walletType),
        "merchantKey": this.merchantKey != "" ? this.merchantKey : 0,
        "customersKey": this.customersKey,
        "paymentType": parseInt(this.paymentType),
        "totalGross": parseFloat(this.totalGross)
      };

      var headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + this.token });
      let url = environment.backendserver.updateDeliveryStatus;


      this._http.post(url, data, { headers: headers })
        .subscribe(res => {

          this._db.object('deliveries/' + this.customersKey + '/' + this.deliveryFireBaseID).update({
            cancellationReason: this.myReason,
            status: '5',
            zzztimecancelled: currendate,
          })
          this._db.object('promoUsage/' + this.promoCode + '/' + this.customersKey).remove()

          this.dialog.closeAll();

        }, (err) => {
          console.log(err);
        });

    }
  }

  enterReason() {
    this.myReason = null;

  }

  enterFee() {
    this.myReason = null;
  }

  submitArrived(ref: TemplateRef<any>) {
    this.dialog.closeAll();
    this.dialog.open(ref, { disableClose: true });
  }

  closeArrived() {
    this.dialog.closeAll();
  }

  close() {
    this.dialog.closeAll();
  }

  cancelOrder(deliveryFireBaseID) {

  }

  openDialogWithRef(ref: TemplateRef<any>) {
    this.dialog.open(ref, { disableClose: true });
  }

}
