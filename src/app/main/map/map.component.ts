import { Component, OnInit } from '@angular/core';


@Component({
	selector: 'app-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
	latitude: number;
	longitude: number;
	zoom: number;
	stops: any;
	markers: any[] = [];
	drivers: any;
	vehicleTypeID: any;

	public markersIcon = [
		{ url: 'assets/markers/From.png',scaledSize:{height: 55, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/1.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/2.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/3.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/4.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/5.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/6.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/7.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/8.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/9.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/10.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/11.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/12.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/13.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/14.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/15.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/16.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/17.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/18.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/19.png',scaledSize:{height: 50, width: 50},labelOrigin: {x: 25,y: -5}},
	  ];

	  public driversIcon = [
		{ url: 'assets/markers/To Pickup -  Motorcycle.svg',scaledSize:{height: 55, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/To Pickup -  Motorcycle.svg',scaledSize:{height: 55, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/To Pickup - MV Small.svg',scaledSize:{height: 55, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/To Pickup - MV Medium.svg',scaledSize:{height: 55, width: 50},labelOrigin: {x: 25,y: -5}},
		{ url: 'assets/markers/To Pickup - MV Large.svg',scaledSize:{height: 55, width: 50},labelOrigin: {x: 25,y: -5}},
	  ];



	constructor() { }

	ngOnInit() {
		//	this.setCurrentLocation();

	}

	

	getDriverLocation($event) {
		this.drivers = $event;

		this.latitude = this.drivers[0];
		this.longitude = this.drivers[1];
		this.vehicleTypeID = this.drivers[2];
	}

	loadStops($event) {
		this.stops = JSON.parse($event);

		for (let x = 0; x < this.stops.length; x++) {
			let lat = this.stops[x].lat;
			let lng = this.stops[x].lng;
			if (lat != '' && lng != '') {
				this.markers.push(JSON.parse('{"index":' + x + ', "lat":"' + lat + '", "lng":"' + lng + '"}'));
			}
		}
	}

	// Get Current Location Coordinates
	private setCurrentLocation() {
		if ('geolocation' in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				this.latitude = position.coords.latitude;
				this.longitude = position.coords.longitude;
				//	this.zoom = 15;

			});
		}
	}

}
