import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from 'src/environments/environment';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { interval, Subscription, ObjectUnsubscribedError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as firebase from 'firebase';
import { FormControl } from '@angular/forms';
import { AuthService } from 'src/services/auth.service';



@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.scss']
})
export class BookingListComponent implements OnInit {

  customerKey: any;
  public items: any;
  status: any;
  intervalId: number;
  public subscription: Subscription;
  getservertime: any;

  displayedColumns: string[] = ['orderNo', 'status', 'deliveryTime', 'totalGross', 'vehicleTypeID', 'deliveryType', 'driverName'];
  dates: string[] = [];

  public dataSource: any;



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public _db: AngularFireDatabase,
    public _route: Router,
    public _http: HttpClient,
    public _dialog: MatDialog,
    public _authService: AuthService
  ) {
    this.customerKey = this._authService.decryptcustomerKey();
    this.loadOrders();
   
  }


  getRecord(row) {
    
    this._route.navigate(['main/map/booking-details'], { state: { deliveryFireBaseID: row.deliveryFireBaseID } });

  }

  applyFilter(filterValue: string) {

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue.toLowerCase();
    
  }


  loadOrders() {

    this._db.list('deliveries/' + this.customerKey).valueChanges()
      .subscribe(result => {
        this.items = result;

        this.dataSource = new MatTableDataSource(this.items);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      
      })

  }

  ngOnInit() {
  
    var customerKey = this._authService.decryptcustomerKey();
    const source = interval(2000);
    this.subscription = source.subscribe(val => this.checkScheduled(customerKey))
  }

  createFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
      console.log(filterFunction);
      let searchTerms = JSON.parse(filter);
      return data.zzorderid.indexOf(searchTerms.zzorderid) !== -1
    }
    return filterFunction;
  }



  checkScheduled(customerKey) {

    var walletType = this._authService.decryptwalletType();
    var merchantKey = this._authService.decryptmerchantId();
    var token = this._authService.decryptToken();
    var subscription = this.subscription;
    var db = this._db;
    var http = this._http;
    var dialog = this._dialog;
    var ordersRef = firebase.database().ref('deliveries/' + customerKey).orderByChild("status").equalTo("1");

    ordersRef.on("child_added", function (data) {

      if (data.val().deliveryType == 1) {
        http.get('http://webadmin.happymove.com.ph/api/utilities/getcurrenttimestamp.php')
          .subscribe(res => {
            var currDate = new Date(res['timestamp']);
            var reqDate = new Date(data.val().deliveryTime);
            reqDate.setMinutes(reqDate.getMinutes() - 60);

            if (currDate >= reqDate) {
              subscription && subscription.unsubscribe();


              let json = {
                "deliveryFireBaseID": data.val().deliveryFireBaseID,
                "status": 5,
                "cancellationReason": 'Booking Expired',
                "walletType": parseInt(walletType),
                "merchantKey": merchantKey != "" ? merchantKey : 0,
                "customersKey": customerKey,
                "paymentType": parseInt(data.val().paymentType),
                "totalGross": parseFloat(data.val().totalGross)
              };
              console.log(json);
              var headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token });
              let url = environment.backendserver.updateDeliveryStatus;

              http.post(url, json, { headers: headers })
                .subscribe(res => {
                  db.object('deliveries/' + customerKey + '/' + data.val().deliveryFireBaseID).update({
                    status: '5'
                  })
                })
            }

          })
      }

    })

  }

}
//