import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-wallet-ledger',
  templateUrl: './wallet-ledger.component.html',
  styleUrls: ['./wallet-ledger.component.scss']
})
export class WalletLedgerComponent implements OnInit {

  displayedColumns: string[] = ['referenceNumber','flagName', 'startingBalance', 'transactionAmount', 'endingBalance', 'remarks','createdOn'];

  public dataSource: any;
  items: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private _http: HttpClient
  ) { }

  ngOnInit() {
    this.loadWalletLedger();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  loadWalletLedger() {

    var token = localStorage.getItem('token');
    var customersKey = localStorage.getItem('customerKey');
    var merchantkey = localStorage.getItem('merchantKey');
    var wallettype = localStorage.getItem('walletType');

    let data = {
      "customerskey": customersKey,
      "merchantkey": merchantkey,
      "wallettype": parseInt(wallettype)
    };

    let config = { headers: new HttpHeaders().set('content-type', 'application/json') };
    var headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token });

    if (parseInt(wallettype) == 2) {
      this._http.get(environment.backendserver.getWalletLedger + '?customerskey=' + data.customerskey + '&merchantkey=' + data.merchantkey + '&wallettype=' + data.wallettype, { headers: headers })
        .subscribe(res => {
          this.items = res[0].customerWalletLedger;
      
          this.dataSource = new MatTableDataSource(this.items);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }, (err) => {
          console.log(err);
        
        });
    } else {
      this._http.get(environment.backendserver.getWalletLedger + '?customerskey=' + data.customerskey + '&merchantkey=0&wallettype=' + data.wallettype, { headers: headers })
        .subscribe(res => {
          this.items = res[0].customerWalletLedger;

          this.dataSource = new MatTableDataSource(this.items);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }, (err) => {
          console.log(err);
          
        });
    }
  }
}
