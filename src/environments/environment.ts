//staging
export const environment = {
  production: false,
	firebase: {
	    apiKey: "AIzaSyD0fuMNnDcn1sSspkiBUVdYPl6YMoOUMt8",
	    authDomain: "happy-move-staging.firebaseapp.com",
	    databaseURL: "https://happy-move-staging.firebaseio.com",
	    projectId: "happy-move-staging",
	    storageBucket: "happy-move-staging.appspot.com",
	    messagingSenderId: "111559565438",
	    appId: "1:111559565438:web:bd68118c9aa103db"
	},
	lambda: {
		login: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/login",
		driversreview: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/driversreview",
		registercustomer: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/registercustomer",
		profileupdate: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/profileupdate",
		updatedeliveryitem: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/updatedeliveryitem",
		deliveryitem: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/deliveryitem"
	},
	backendserver: {
		username: 'HMMobileApp',
		password: 'HMBackend19!',
		secretKey: 'HMdevops91!',
		auth: "http://52.221.196.138:8787/api/users/authenticate",
		login: "http://52.221.196.138:8787/api/Customer/Login",
		loggedin: "http://52.221.196.138:8787/api/Customer/LoggedIn",
		insertdelivery: "http://52.221.196.138:8787/api/Customer/InsertDelivery",
		updateDeliveryStatus: "http://52.221.196.138:8787/api/Customer/UpdateDeliveryStatus",
		updateDeliveryPriorityFee: "http://52.221.196.138:8787/api/Customer/UpdateDeliveryPriorityFee",
		getTotalWallet: "http://52.221.196.138:8787/api/Customer/CustomerGetTotalWallet",
		getCreditLedger: "http://52.221.196.138:8787/api/Customer/GetCustomerCredit",
		getWalletLedger: "http://52.221.196.138:8787/api/Customer/GetCustomerWallet",
		checkAllowedPayment: "http://52.221.196.138:8787/api/Customer/CheckAllowedPayment",
		customerForgotPassword: "http://52.221.196.138:8787/api/Customer/CustomerForgotPassword"	
	}
};


//live
// export var environment = {
//     production: false,
//     firebase: {
//         apiKey: "AIzaSyALFKkZkL5JgNzDMdHQEqmu9Uo6-z3ps2M",
// 	       authDomain: "happy-move-217906.firebaseapp.com",
// 	       databaseURL: "https://happy-move-217906.firebaseio.com",
// 	       projectId: "happy-move-217906",
// 	       storageBucket: "happy-move-217906.appspot.com",
// 	       messagingSenderId: "426508096643"
//     },
//     lambda: {
//     	   login: "https://46rpo2oczb.execute-api.ap-southeast-1.amazonaws.com/hm_driverapi_devtest/login",
//     	   driversreview: "https://46rpo2oczb.execute-api.ap-southeast-1.amazonaws.com/hm_driverapi_devtest/driversreview",
//     	   registercustomer: "https://46rpo2oczb.execute-api.ap-southeast-1.amazonaws.com/hm_driverapi_devtest/registercustomer",
//     	   profileupdate: "https://46rpo2oczb.execute-api.ap-southeast-1.amazonaws.com/hm_driverapi_devtest/profileupdate",
//     	   updatedeliveryitem: "https://46rpo2oczb.execute-api.ap-southeast-1.amazonaws.com/hm_driverapi_devtest/updatedeliveryitem",
//     	   deliveryitem: "https://46rpo2oczb.execute-api.ap-southeast-1.amazonaws.com/hm_driverapi_devtest/deliveryitem"
//     },
// 	backendserver: {
// 		username: 'HMMobileApp',
// 		password: 'HMBackend19!',
// 		secretKey: 'HMdevops91!',
// 		auth: "http://52.221.196.138:8989/api/users/authenticate",
// 		login: "http://52.221.196.138:8989/api/Customer/Login",
// 		loggedin: "http://52.221.196.138:8989/api/Customer/LoggedIn",
// 		insertdelivery: "http://52.221.196.138:8989/api/Customer/InsertDelivery",
// 		updateDeliveryStatus: "http://52.221.196.138:8989/api/Customer/UpdateDeliveryStatus",
// 		updateDeliveryPriorityFee: "http://52.221.196.138:8989/api/Customer/UpdateDeliveryPriorityFee",
// 		getTotalWallet: "http://52.221.196.138:8989/api/Customer/CustomerGetTotalWallet",
// 		getCreditLedger: "http://52.221.196.138:8989/api/Customer/GetCustomerCredit",
// 		getWalletLedger: "http://52.221.196.138:8989/api/Customer/GetCustomerWallet",
// 		checkAllowedPayment: "http://52.221.196.138:8989/api/Customer/CheckAllowedPayment",
//      	customerForgotPassword: "http://52.221.196.138:8989/api/Customer/CustomerForgotPassword"
// 	}
// };


//local
// export const environment = {
//   production: false,
// 	firebase: {
// 	    apiKey: "AIzaSyD0fuMNnDcn1sSspkiBUVdYPl6YMoOUMt8",
// 	    authDomain: "happy-move-staging.firebaseapp.com",
// 	    databaseURL: "https://happy-move-staging.firebaseio.com",
// 	    projectId: "happy-move-staging",
// 	    storageBucket: "happy-move-staging.appspot.com",
// 	    messagingSenderId: "111559565438",
// 	    appId: "1:111559565438:web:bd68118c9aa103db"
// 	},
// 	lambda: {
// 		login: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/login",
// 		driversreview: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/driversreview",
// 		registercustomer: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/registercustomer",
// 		profileupdate: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/profileupdate",
// 		updatedeliveryitem: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/updatedeliveryitem",
// 		deliveryitem: "https://1ti3kffdzf.execute-api.ap-southeast-1.amazonaws.com/hm_dev_api/deliveryitem"
// 	},
// 	backendserver: {
// 		username: 'HMMobileApp',
// 		password: 'HMBackend19!',
// 		auth: "http://localhost:5000/api/users/authenticate",
// 		login: "http://localhost:5000/api/Customer/Login",
// 		loggedin: "http://localhost:5000/api/Customer/LoggedIn",
// 		insertdelivery: "http://localhost:5000/api/Customer/InsertDelivery",
// 		updateDeliveryStatus: "http://localhost:5000/api/Customer/UpdateDeliveryStatus",
// 		updateDeliveryPriorityFee: "http://localhost:5000/api/Customer/UpdateDeliveryPriorityFee",
// 		getTotalWallet: "http://localhost:5000/api/Customer/CustomerGetTotalWallet",
// 		getCreditLedger: "http://localhost:5000/api/Customer/GetCustomerCredit",
// 		getWalletLedger: "http://localhost:5000/api/Customer/GetCustomerWallet",
// 		checkAllowedPayment: "http://localhost:5000/api/Customer/CheckAllowedPayment",
// 		customerForgotPassword: "http://localhost:5000/api/Customer/CustomerForgotPassword"
// 	}
// };
